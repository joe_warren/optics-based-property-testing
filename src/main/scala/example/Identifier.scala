package example

case class Identifier(namespace: String, separator: Char, name: String) {
  def fullname: String = namespace :+ separator :++ name
}
