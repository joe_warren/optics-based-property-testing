package example

import monocle.macros.GenLens
import monocle.std.either
import org.scalacheck.{Gen, Properties}
import org.scalacheck.Arbitrary.arbitrary

object LabeledEitherPropertyTestingSpec extends Properties("LabeledEither") with LensDrivenGenerators {

  case class LabeledEither(label: String, value: Either[Int, String]){
    def eitherIsRight: Boolean = value.isRight
    def intValue: Int = value match {
      case Left(i) => i
      case Right(s) => s.length
    }
  }

  val nonEmptyString: Gen[String] = Gen.nonEmptyBuildableOf[String, Char](Gen.alphaChar)

  val label = GenLens[LabeledEither](_.label)
  val value = GenLens[LabeledEither](_.value)

  val rightOfLabel = value.composePrism(either.stdRight[Int, String])

  // This is either Left 1, or a non empty string
  val randomizedLabeledEither = minimalExample(LabeledEither("label", Left(0)))
    .randomize(label, arbitrary[String])
    .randomize(value, Gen.oneOf(Left(1), Right("")))
    .randomize(rightOfLabel, nonEmptyString)

  // since we're either Left(1) or Right(nonEmptyString), intValue > 0
  property("int value shall be positive") = randomizedLabeledEither.forAll(_.intValue > 0)

  // this demonstraits that you can use the same prism for setting, and for adding conditions to the
  property("all right values are right") = randomizedLabeledEither.where(rightOfLabel).forAll(_.eitherIsRight)

  property("all labels called name are calledName")  = randomizedLabeledEither.fixing(label, "name").forAll(_.label == "name")
}
