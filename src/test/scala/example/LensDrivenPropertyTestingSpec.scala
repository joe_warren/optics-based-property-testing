package example

import monocle.macros.GenLens
import monocle.std.either
import monocle.{Lens, PPrism, Prism}
import org.scalacheck._
import org.scalacheck.Prop.forAll
import org.scalacheck.Arbitrary.arbitrary
import org.scalacheck.util.Pretty
import org.scalatest.Matchers
import org.scalatest.flatspec.AnyFlatSpec

object LensDrivenPropertyTestingSpec extends Properties("Identifier") with LensDrivenGenerators {

  val namespaceLens = GenLens[Identifier](_.namespace)
  val nameLens = GenLens[Identifier](_.name)

  val example = minimalExample(Identifier("namespace", ':', "name"))

  val withRandomizedName = example.randomize(nameLens, arbitrary[String])

  val bothNamesRandomized = withRandomizedName.randomize(namespaceLens, arbitrary[String])

  property("Starts with namespace") = withRandomizedName.forAll(_.fullname.startsWith("namespace:"))

  property("Bigger than the sum of it's parts") = bothNamesRandomized.forAll{ identifier =>
    identifier.fullname.length > identifier.namespace.length + identifier.name.length
  }


}
