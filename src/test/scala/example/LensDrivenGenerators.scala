package example

import cats.Monad
import monocle._
import monocle.macros.GenLens
import org.scalacheck.util.Pretty
import org.scalacheck.{Arbitrary, Gen, Prop, Shrink}

import scala.annotation.tailrec

trait LensDrivenGenerators {
  // scalacheck Gen isn't a cats monad out of the box
  implicit def genIsMonad = new Monad[Gen] {
    override def pure[A](x: A): Gen[A] = Gen.const(x)

    override def flatMap[A, B](fa: Gen[A])(f: A => Gen[B]): Gen[B] = fa.flatMap(f)

    override def tailRecM[A, B](a: A)(f: A => Gen[Either[A, B]]): Gen[B] = f(a).flatMap  {
      case Right(b)    => Gen.const(b)            // recursion done
      case Left(nextA) => tailRecM(nextA)(f) // continue the recursion
    }
  }
  // for generality, write most of the dsl in terms of traversals, and add implicits from optics to the traversals
  implicit def lensIsTraversal[S, T, A, B](lens: PLens[S, T, A, B]): PTraversal[S, T, A, B] = lens.asTraversal
  implicit def prismIsTraversal[S, T, A, B](prism: PPrism[S, T, A, B]): PTraversal[S, T, A, B] = prism.asTraversal
  implicit def optionalIsTraversal[S, T, A, B](optional: POptional[S, T, A, B]): PTraversal[S, T, A, B] = optional.asTraversal

  // alias Gen.const
  def minimalExample[A]: A => Gen[A] = Gen.const[A]


  implicit class RichGen[S](gen: Gen[S]){
    // lens driven generator dsl
    def modifyP[T, A, B](traversal: PTraversal[S, T, A, B], transform: A => Gen[B]): Gen[T] = gen.flatMap(traversal.modifyF(transform)(_))
    def modify[A](traversal: Traversal[S, A], transform: A => Gen[A]): Gen[S] = modifyP(traversal, transform)
    def randomize[A](traversal: Traversal[S, A], focus: Gen[A]) = modify[A](traversal, {case _:A => focus})
    def fixing[A](traversal: Traversal[S, A], focus: A) = randomize(traversal, Gen.const(focus))

    // optional driven generator dsl
    def where[T, A, B](optional: POptional[S, T, A, B]) = gen.suchThat{optional.nonEmpty(_)}

    // alias for Prop.forAll to make it easier to run properties on a specific generator, without going via an Arbitrary
    def forAll[B](f: S => B)(implicit propFn: B=>Prop, shrink: Shrink[S], pretty: S => Pretty): Prop =
      Prop.forAll(f)(propFn, Arbitrary(gen), shrink, pretty)
  }

}
