# Optics Driven Property Testing

This is a PoC for the idea that Optics and Property Based Testing might combine to make a decent testing paradigm. 

With typical property based testing, you have to create Generators for the entire datatype under test.
This is often arduous. 

Combining property based testing with an optics framework, such as monocle, lets you start with a minimal example object, and then focus on the parts of the structure that you want to vary.

This may or may not be a good idea; it's early days. 

